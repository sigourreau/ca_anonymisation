global class AP10_UserAnonymization implements Schedulable {
	public void execute (SchedulableContext context){
		String fieldList = Label.FieldsListToEncrypt;
		List<String> listFields = fieldList.split(';');
		String query = 'Select '
						+ 'Id, '
						+ 'Matricule__c, '
						+ 'FirstName, '
						+ 'LastName, '
						+ 'Alias, '
						+ 'CommunityNickname, '
						+ 'Username, '
						+ 'FullPhotoUrl, '
						+ 'BannerPhotoUrl';
		for(String field : listFields){
		    query += ', ' + field;
		}

		query += ' From User '
				+ 'Where TECH_AnonymisationStatus__c = \'1\' '
				+ 'And IsActive = true '
				+ 'And Provisioning__c = true '
				+ 'And Email = \'' + Label.AP03_OrgWideEmailAddress + '\'';
		System.debug('## AP10_UserAnonymization query ' + query);

		List<User> listUsers = (List<User>)Database.query(query);
		System.debug('## AP10_UserAnonymization listUsers ' + listUsers);

		AP01User.anonymizeUsers(listUsers);
	}
}