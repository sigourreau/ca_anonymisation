@isTest
public class AP01User_TEST {
    static testMethod void assignProfileTest(){
        PAD.IsEmpty('notEmpty');
        PAD.IsEmpty('');
        System.debug('AP01User_Test DEBUG >>> run by user :'+PAD.getUserInfo().Id);
        Profile p = [select id from Profile where name='Chatter Free User' limit 1];
        Profile profileExpected = [select id from Profile where name=:Label.ProfileChatterOnlyAll];
        UserRole userRoleExpected = [select id from UserRole where name=:Label.RoleToAssign];
        System.debug('AP01User_Test DEBUG >>> Profile id selected :'+p.Id);
        String[] domainsAuthorized = Label.DomainNameAuthorizedForProfileAssignment.split(';');
        System.debug('AP01User_Test DEBUG >>> domainsAuthorized :'+domainsAuthorized);
        
        User u = new User(
            ProfileId = p.Id,
            email = 'test@'+domainsAuthorized[0],
            username = 'test@'+domainsAuthorized[0],
            LastName = 'TEST',
            Alias = 'alias123',
            TimeZoneSidKey = 'Europe/Paris',
            localesidkey='fr_FR',
			emailencodingkey='UTF-8',
			languagelocalekey='fr'
        );
        insert u;
        u = [select id, ProfileId, UserRoleId from User where id=:u.Id limit 1];
        System.assert(u.ProfileId==profileExpected.Id);
        //System.assert(u.UserRoleId==userRoleExpected.Id);
    }

    @testSetup 
    static void dataCreation() {
        User adminUser = DataFactory.getAdminUser('testAdmin@ap01.com');
        Database.insert(adminUser);

        User chatterUser = DataFactory.getChatterUser('testChatter@ap01.com');
        Database.insert(chatterUser);

        User chatterUserReadyToAnonymize = DataFactory.getChatterUser('testchatteruserreadytoanonymize@ap01.com');
        chatterUserReadyToAnonymize.TECH_AnonymisationStatus__c = '1';
        chatterUserReadyToAnonymize.Provisioning__c = true;
        chatterUserReadyToAnonymize.Email = Label.AP03_OrgWideEmailAddress;
        chatterUserReadyToAnonymize.Matricule__c = '123123';
        chatterUserReadyToAnonymize.AboutMe = 'It\'s me';
        Database.insert(chatterUserReadyToAnonymize);
    }

    @isTest
    static void preAnonymize(){
        User adminUser = [Select Id From User Where UserName = 'testAdmin@ap01.com'];
        System.runAs(adminUser){
            User chatterUser = [Select Id From User Where UserName = 'testChatter@ap01.com'];
            chatterUser.TECH_AnonymisationStatus__c = '1';
            Test.startTest();
            Database.update(chatterUser);
            Test.stopTest();

            User chatterUserAfterUpd = [Select Id, TECH_EncryptedEmail__c From User Where UserName = 'testChatter@ap01.com'];
            System.assertNotEquals(chatterUserAfterUpd.TECH_EncryptedEmail__c, null);
        }
    }

    @isTest
    static void anonymize(){
        User adminUser = [Select Id From User Where UserName = 'testAdmin@ap01.com'];
        System.runAs(adminUser){
            String fieldList = Label.FieldsListToEncrypt;
            List<String> listFields = fieldList.split(';');
            String query = 'Select '
                            + 'Id, '
                            + 'Matricule__c, '
                            + 'FirstName, '
                            + 'LastName, '
                            + 'Alias, '
                            + 'CommunityNickname, '
                            + 'Username, '
                            + 'FullPhotoUrl, '
                            + 'BannerPhotoUrl';
            for(String field : listFields){
                query += ', ' + field;
            }

            query += ' From User '
                    + 'Where UserName = \'testchatteruserreadytoanonymize@ap01.com\'';

            List<User> listUsers = (List<User>)Database.query(query);

            System.debug(query);
            System.debug(listUsers);

            Test.startTest();
            AP01User.anonymizeUsers(listUsers);
            Test.stopTest();

            User userAfterAnonimyze = [Select Id, Username, TECH_AnonymisationStatus__c From User Where Matricule__c = '123123'];

            System.assertNotEquals(listUsers[0].Username, userAfterAnonimyze.Username, 'Usernames are equals');
            System.assertEquals(userAfterAnonimyze.TECH_AnonymisationStatus__c, '2', 'Status code equals ' + userAfterAnonimyze.TECH_AnonymisationStatus__c);
        }
    }

    @isTest
    static void unanonymize(){
        User adminUser = [Select Id From User Where UserName = 'testAdmin@ap01.com'];
        System.runAs(adminUser){
            String fieldList = Label.FieldsListToEncrypt;
            List<String> listFields = fieldList.split(';');
            String query = 'Select '
                            + 'Id, '
                            + 'Matricule__c, '
                            + 'FirstName, '
                            + 'LastName, '
                            + 'Alias, '
                            + 'CommunityNickname, '
                            + 'Username, '
                            + 'FullPhotoUrl, '
                            + 'BannerPhotoUrl';
            for(String field : listFields){
                query += ', ' + field;
            }

            query += ' From User '
                    + 'Where UserName = \'testchatteruserreadytoanonymize@ap01.com\'';

            List<User> listUsers = (List<User>)Database.query(query);
            String aboutMe = listUsers[0].AboutMe;

            System.debug(query);
            System.debug(listUsers);

            Test.startTest();
            AP01User.anonymizeUsers(listUsers);
            Test.stopTest();

            User userAfterAnonimyze = [Select Id, TECH_AnonymisationStatus__c From User Where Matricule__c = '123123'];
            userAfterAnonimyze.TECH_AnonymisationStatus__c = '0';

            Database.update(userAfterAnonimyze);

            User userAfterUnanonimyze = [Select Id, TECH_AnonymisationStatus__c, AboutMe From User Where Matricule__c = '123123'];

            System.assertEquals(userAfterUnanonimyze.AboutMe, aboutMe);
        }
    }
}