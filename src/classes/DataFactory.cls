/******************************************************************************
*   @author Simon Gourreau
*   @date 20/09/2017
*   @description Class which generate all elements to insert
*	to allow modifications in one place
*
*/
public with sharing class DataFactory {

	/**
    * @description Create new user with administrator profile
    * @param String : username
    * @return User create
    *
    */
    public static user getAdminUser(String userName){
        Profile p = [SELECT Id 
                        FROM Profile 
                        WHERE Name='System Administrator'
                        OR Name='Administrateur système'];
        return new User(Alias = 'sadm', Email = 'adminuser@testorg.com',
          EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'fr',
          LocaleSidKey = 'fr_FR_EURO', ProfileId = p.Id,
          TimeZoneSidKey = 'Europe/Paris', UserName = userName);
    }

    /**
    * @description Create new user with chatter only user all profile
    * @param String : username
    * @return User create
    *
    */
    public static user getChatterUser(String userName){
        Profile p = [SELECT Id 
                        FROM Profile 
                        WHERE Name='Chatter Only User All'];
        return new User(Alias = 'sadm', Email = 'chatteruser@testorg.com',
          EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'fr',
          LocaleSidKey = 'fr_FR_EURO', ProfileId = p.Id,
          TimeZoneSidKey = 'Europe/Paris', UserName = userName);
    }
}