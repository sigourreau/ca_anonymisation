/*
* @author 		: Simon GOURREAU
* @date 		: 20/09/2017
* @description	: Utility class to encrypte and decrypt strings
*/
public with sharing class AP07_EncryptionDecryption {

	/**
    * @description Encrypte a string with a private key
    * @param String
    * @return String encrypted
    *
    */
	public static String encrypte(String strToEncrypt) {
		String key = Label.CryptoKey;
		Blob secretKey = Blob.valueOf(key);
		Blob clearData = Blob.valueOf(strToEncrypt);
		Blob encryptedData = Crypto.encryptWithManagedIV('AES128', secretKey, clearData);
		String blobStr = EncodingUtil.base64Encode(encryptedData);

		System.debug('## AP07_EncryptionDecryption.encrypte strToEncrypt : ' + strToEncrypt);
		System.debug('## AP07_EncryptionDecryption.encrypte blobStr : ' + blobStr);

		return blobStr;
	}

	/**
    * @description Decrypte a string with a private key
    * @param String
    * @return String decrypted
    *
    */
	public static String decrypte(String strToDecrypt) {
		String key = Label.CryptoKey;
		Blob secretKey = Blob.valueOf(key);
		Blob clearData = EncodingUtil.base64Decode(strToDecrypt);
		Blob decryptedData = Crypto.decryptWithManagedIV('AES128', secretKey, clearData);
		String blobStr = decryptedData.toString();

		System.debug('## AP07_EncryptionDecryption.decrypte strToDecrypt : ' + strToDecrypt);
		System.debug('## AP07_EncryptionDecryption.decrypte blobStr : ' + blobStr);

		return blobStr;
	}
}