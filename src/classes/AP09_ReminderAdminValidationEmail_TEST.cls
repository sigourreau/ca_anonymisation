/******************************************************************************
*   @author Simon Gourreau
*   @date 27/09/2017
*   @description AP09_ReminderAdminValidationEmail test class
*
*/
@isTest
private class AP09_ReminderAdminValidationEmail_TEST {
	@testSetup 
	static void dataCreation() {
		User adminUser = DataFactory.getAdminUser('testadmin@ReminderAdminValidationEmail.com');
		Database.insert(adminUser);

		/*User chatterUser = DataFactory.getChatterUser('testchatter@ReminderAdminValidationEmail.com');
		chatterUser.Provisioning__c = true;
		chatterUser.TECH_AnonymisationStatus__c = '1';
		Database.insert(chatterUser);*/
	}

	static testMethod void AP09_ReminderAdminValidationEmail_TEST() {
		User adminUser = [Select Id From User Where UserName = 'testadmin@ReminderAdminValidationEmail.com'];
		System.runAs(adminUser){
			Test.startTest();
		    System.schedule('AP09_ReminderAdminValidationEmail', '0 0 0 * * ?', new AP09_ReminderAdminValidationEmail());
		    Test.stopTest();
		}
	}
}