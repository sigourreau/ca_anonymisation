/*
* @author 		: Simon GOURREAU
* @date 		: 20/09/2017
* @description	: Test class for AP07_EncryptionDecryption class
*/
@isTest
private class AP07_EncryptionDecryption_Test{
	
	@testSetup 
	static void dataCreation() {
		User user = DataFactory.getAdminUser('test@encryption.com');
		Database.insert(user);
	}

	@isTest
	static void encryption(){
		User user = [Select Id From User Where UserName = 'test@encryption.com'];
        System.runAs(user){
        	String valueToEncrypte = 'My encryption test';
        	String encryptionResult = '';
        	String decryptionResult = '';
        	Test.startTest();
        	encryptionResult = AP07_EncryptionDecryption.encrypte(valueToEncrypte);
        	decryptionResult = AP07_EncryptionDecryption.decrypte(encryptionResult);
        	Test.stopTest();

        	System.assertEquals(valueToEncrypte, decryptionResult);
        }
	}
}