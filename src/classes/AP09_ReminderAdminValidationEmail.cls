/*
* @author 		: Simon GOURREAU
* @date 		: 27/09/2017
* @description	: Batch to schedule : Change user email to orgwide mail if admin has not validated the email 
*/
global class AP09_ReminderAdminValidationEmail implements Schedulable {
	
	/**
     *  @description Execute schedulable process
     *  @param SchedulableContext
     */
	public void execute (SchedulableContext context){
		Integer nbDays = Integer.valueOf(Label.NbDaysToRemindeAdmin);
		List<User> listUsers = [Select Id, Email
								From User
								Where Provisioning__c = true
								And Email != :Label.AP03_OrgWideEmailAddress
								And TECH_AnonymisationStatus__c = '1'
								And LastModifiedDate < :Datetime.now().addDays(-nbDays)];

		System.debug('## AP09_ReminderAdminValidationEmail listUsers ' + listUsers);

		List<User> listUsersToReminder = new List<User>();
		for(User us : listUsers){
			us.Email = Label.AP03_OrgWideEmailAddress;
			listUsersToReminder.add(us);
		}

		System.debug('## AP09_ReminderAdminValidationEmail listUsersToReminder ' + listUsersToReminder);

		Database.update(listUsersToReminder);
	}
}