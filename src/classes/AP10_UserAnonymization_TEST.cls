/******************************************************************************
*   @author Simon Gourreau
*   @date 27/09/2017
*   @description AP10_UserAnonymization test class
*
*/
@isTest
private class AP10_UserAnonymization_TEST {
	@testSetup 
	static void dataCreation() {
		User adminUser = DataFactory.getAdminUser('testadmin@UserAnonymization.com');
		Database.insert(adminUser);
	}

	static testMethod void AP10_UserAnonymization_TEST() {
		User adminUser = [Select Id From User Where UserName = 'testadmin@UserAnonymization.com'];
		System.runAs(adminUser){
			Test.startTest();
		    System.schedule('AP10_UserAnonymization', '0 0 0 * * ?', new AP10_UserAnonymization());
		    Test.stopTest();
		}
	}
}