/**
* Author 		: Pascal Le Clech 
* Date 			: 21/04/2016
* Test Class 	: AP01User_TEST
* 
* Description	: 
* 		assignProfile is a method to automatically assign a profile to new users
* 		based on their email addresses.		 		
*
* Update
* @author       : Simon GOURREAU
* @date         : 20/09/2017
* @description  : encrypte ou decrypte user data if flag checked or not
*/

public class AP01User {
    
    // mode values : 'profile', 'role', 'profilerole'
    // mode = profile then only profile is changed
    // mode = role then only role is changed
    // mode = profilerole then both are changed
    // toUpdate true if we need to update User List (used in triggers after insert)
    
    public static void assignProfile(List<User> users,String mode,boolean toUpdate){
        Profile[] p = [select id from Profile where name=:Label.ProfileChatterOnlyAll limit 1];
        UserRole[] ur = [select id from UserRole where name=:Label.RoleToAssign limit 1];
        boolean condition = (mode=='profile'&&p.size()>0)||
            				(mode=='role'&&ur.size()>0) ||
            				(mode=='profilerole'&&p.size()>0&&ur.size()>0);
        if(condition){
            for(User u : users){
                String[] emailAddressSplited = u.Email.split('@');
                System.debug('AP01User DEBUG >>> email address splited :'+emailAddressSplited);
                String domainName;
                if(emailAddressSplited.size()>1)
                	domainName = emailAddressSplited[1];
                System.debug('AP01User DEBUG >>> domainName :'+domainName);
                String[] domainsAuthorized = Label.DomainNameAuthorizedForProfileAssignment.split(';');
                System.debug('AP01User DEBUG >>> domainsAuthorized :'+domainsAuthorized);
                Set<String> setDomainsAuthorized = new Set<String>(domainsAuthorized);
                System.debug('AP01User DEBUG >>> setDomainsAuthorized :'+setDomainsAuthorized);
                if(setDomainsAuthorized.contains(domainName)){
                    if(mode=='profile'||mode=='profilerole')
                    	u.ProfileId = p[0].id;
                    if((mode=='role'||mode=='profilerole')&&u.UserType=='Standard')
                    	u.UserRoleId = ur[0].id;
                }
            } 
        }
        if(toUpdate){
            update users;
        }
    }

    /**
     *  @description Pre-anonymize users which have status to 0 switch to 1 :
     *  Active user and change e-mail to validate before unactive it
     *  @param List<User> : users in trigger in
     *  @param Map<Id, User> : old users
     *  @param Map<Id, User> : new users
     */
    public static void preAnonymizeUsers(List<User> listUsers, Map<Id, User> mapIdUsersOld, Map<Id, User> mapIdUsersNew){
        for(User us : listUsers){
            if((mapIdUsersOld.get(us.Id).TECH_AnonymisationStatus__c == '0'
                    || String.isBlank(mapIdUsersOld.get(us.Id).TECH_AnonymisationStatus__c))
                    && mapIdUsersNew.get(us.Id).TECH_AnonymisationStatus__c == '1'){
                System.debug('## preAnonymizeUsers old value us.Email : ' + us.Email);
                us.IsActive = true;
                us.TECH_EncryptedEmail__c = AP07_EncryptionDecryption.encrypte(us.Email);
                us.Email = Label.AP03_OrgWideEmailAddress;
            }
        }
    }

    /**
     *  @description Anonymize users : encrypte data in document and clear non-mandatory fields
     *  @param List<User>
     */
    public static void anonymizeUsers(List<User> listUsers){
        List<User> userToUpdate = new List<User>();
        for(User us : listUsers){
            String fieldList = Label.FieldsListToEncrypt;
            List<String> listFields = fieldList.split(';');
            
            String body = constructDocumentBody(us, listFields);

            List<Folder> listFolders = [Select Id 
                                        From Folder 
                                        Where DeveloperName = :Label.DocFolderName
                                        Limit 1];

            if(listFolders.size() > 0){
                String encryptedMatricule = '';
                if(String.isNotBlank(us.Matricule__c)){
                    encryptedMatricule = AP07_EncryptionDecryption.encrypte(us.Matricule__c);
                } else {
                    System.debug('## User has no matricule');
                }
                createDocument(listFolders[0].Id, 
                                body, 
                                encryptedMatricule,
                                us.FullPhotoUrl,
                                us.BannerPhotoUrl);

                System.debug('## AnonymizeUsers old value FirstName : ' + us.FirstName
                                + ', LastName : '                        + us.LastName
                                + ', Alias : '                           + us.Alias
                                + ', CommunityNickname : '               + us.CommunityNickname
                                + ', Username : '                        + us.Username);

                for(String field : listFields){
                    if(checkIfFieldExists(field)){
                        us.put(field, null);
                    }
                }

                us.FirstName = 'Utilisateur';
                us.LastName = 'Anonyme';
                us.Alias = 'uanonym';
                us.CommunityNickname = (AP07_EncryptionDecryption.encrypte(us.CommunityNickname)).right(40);
                us.TECH_AnonymisationStatus__c = '2';
                us.Username = (encryptedMatricule + '@'
                                + us.Username.split('@')[1]).right(80);
                us.IsActive = false;
                us.TECH_EncryptedMatricule__c = encryptedMatricule;

                List<Document> listDocPhoto = [Select Id, Body From Document Where Name = :Label.DocPhotoAnonymousName];
                List<Document> listDocBanner = [Select Id, Body From Document Where Name = :Label.DocBannerAnonymousName];
                if(listDocPhoto.size() == 1){
                    updateProfilePhoto(us.Id, listDocPhoto[0].Body);
                }

                if(listDocBanner.size() == 1){
                    updateBannerPhoto(us.Id, listDocBanner[0].Body);
                }

                userToUpdate.add(us);
            }
        }

        if(userToUpdate.size() > 0){
            Database.update(userToUpdate);
        }
    }

    /**
     *  @description "Unanonymize" users which have TECH_AnonymisationStatus__c field change from 2 to 0
     *  with decryption informations
     *  @param List<User> : users in trigger in
     *  @param Map<Id, User> : old users
     *  @param Map<Id, User> : new users
     */
    public static void unanonymizeUsers(List<User> listUsers, Map<Id, User> mapIdUsersOld, Map<Id, User> mapIdUsersNew){
        System.debug('## Unanonymize Users');
        for(User us : listUsers){
            if(mapIdUsersOld.get(us.Id).TECH_AnonymisationStatus__c == '2' 
                && mapIdUsersNew.get(us.Id).TECH_AnonymisationStatus__c == '0'){
                List<Folder> listFolders = [Select Id 
                                            From Folder 
                                            Where DeveloperName = :Label.DocFolderName
                                            Limit 1];
                if(listFolders.size() > 0){
                    List<Document> listDocuments = [Select Id, Body
                                                    From Document 
                                                    Where Name = :us.TECH_EncryptedMatricule__c
                                                    And FolderId = :listFolders[0].Id
                                                    Limit 1];

                    if(listDocuments.size() > 0){
                        String fieldsAndValuesList = listDocuments[0].Body.toString();
                        if(String.isNotBlank(fieldsAndValuesList)){
                            List<String> listFieldsAndValues = fieldsAndValuesList.split(';');
                            Map<String, String> mapFieldsValues = new Map<String, String>();
                            for(String fieldAndValue : listFieldsAndValues){
                                List<String> listFieldsValues = new List<String>();
                                listFieldsValues = fieldAndValue.split(':');
                                if(listFieldsValues.size() == 2){
                                    mapFieldsValues.put(listFieldsValues[0], listFieldsValues[1]);
                                }
                            }

                            String fieldList = Label.FieldsListToEncrypt;
                            List<String> listField = fieldList.split(';');
                            for(String field : listField){
                                if(checkIfFieldExists(field)){
                                    if(String.isNotBlank(mapFieldsValues.get(field))){
                                        // If there is no value yet
                                        if(String.isBlank((String)us.get(field))){
                                            System.debug('## unanonymizeUsers field : ' + field);
                                            System.debug('## unanonymizeUsers mapFieldsValues.get(field) : ' + mapFieldsValues.get(field));
                                            System.debug('## unanonymizeUsers mapFieldsValues.get(field) decrypted : ' + AP07_EncryptionDecryption.decrypte(mapFieldsValues.get(field)));
                                            us.put(field, AP07_EncryptionDecryption.decrypte(mapFieldsValues.get(field)));
                                        }
                                    }
                                }
                            }
                            Blob photo = EncodingUtil.base64Decode(mapFieldsValues.get('Photo'));
                            Blob banner = EncodingUtil.base64Decode(mapFieldsValues.get('Banner'));

                            if(!Test.isRunningTest()){
                                updateProfilePhoto(us.Id, photo);
                                updateBannerPhoto(us.Id, banner);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *  @description Update user profile photo with a blob
     *  @param String : user id
     *  @param Blob : photo
     */
    @future
    private static void updateProfilePhoto(String userId, Blob photo){
        System.debug('## Update profile photo');
        ConnectApi.UserProfiles.setPhoto(null, 
                                            userId, 
                                            new ConnectApi.BinaryInput(photo, 
                                                                        'image/jpg', 
                                                                        'userImage.jpg'));
    }

    /**
     *  @description Update user profile banner with a blob
     *  @param String : user id
     *  @param Blob : photo
     */
    @future
    private static void updateBannerPhoto(String userId, Blob photo){
        System.debug('## Update banner photo');
        ConnectApi.UserProfiles.setBannerPhoto(null, 
                                                userId, 
                                                new ConnectApi.BinaryInput(photo, 
                                                                            'image/jpg', 
                                                                            'userImage.jpg'));
    }

    /**
     *  @description Create and insert a document
     *  @param String : folder Id
     *  @param String : document body
     *  @param String : document name
     *  @param String : user photo url
     *  @param String : user banner url
     */
    @future(callout=true)
    private static void createDocument(String folderId, String body, String name, String userPhotoUrl, String userBannerUrl){
        Blob photoContent;
        Blob bannerContent;

        PageReference pageRefPhoto = new PageReference(userPhotoUrl);
        PageReference pageRefBanner = new PageReference(userBannerUrl);

        if(Test.isRunningTest()){
            photoContent = Blob.valueOf('Test Photo');
            bannerContent = Blob.valueOf('Test Banner');
        } else {
            photoContent = pageRefPhoto.getContent();
            bannerContent = pageRefBanner.getContent();
        }

        body += 'Photo:' + EncodingUtil.base64Encode(photoContent) + ';';
        body += 'Banner:' + EncodingUtil.base64Encode(bannerContent) + ';';

        Document doc = new Document();
        doc.FolderId = folderId;
        doc.Body = Blob.valueOf(body);
        doc.Name = name;
        Database.insert(doc);
    }

    /**
     *  @description Construct body of the document
     *  @param User
     *  @param List<String>
     *  @return String
     */
    private static String constructDocumentBody(User us, List<String> listFields){
        String body = '';
        for(String field : listFields){
            if(checkIfFieldExists(field)){
                String fieldValue = (String)us.get(field);
                if(String.isNotBlank(fieldValue)){
                    body += field + ':' + AP07_EncryptionDecryption.encrypte(fieldValue) + ';';
                }
            }
        }

        return body;
    }

    /**
     *  @description Check if field in exists in user object
     *  @param String
     *  @return Boolean
     */
    private static Boolean checkIfFieldExists(String fieldName){
        Set<String> objectFields = Schema.SObjectType.User.fields.getMap().keySet();
        Boolean isFieldExists = false;
        if(objectFields.contains(fieldName)) {
            isFieldExists = true;
        } else {
            System.debug('## AP01User.checkIfFieldExists this field does not exists : ' + fieldName);
        }

        return isFieldExists;
    }
}