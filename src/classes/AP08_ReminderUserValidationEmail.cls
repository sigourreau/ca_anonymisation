/*
* @author 		: Simon GOURREAU
* @date 		: 27/09/2017
* @description	: Batch to schedule : Change user email to encrypted its old one if he has not validated the email 
*/
global class AP08_ReminderUserValidationEmail implements Schedulable {
	
	/**
     *  @description Execute schedulable process
     *  @param SchedulableContext
     */
	public void execute (SchedulableContext context){
		List<User> listUsers = [Select Id, TECH_EncryptedEmail__c
								From User
								Where IsActive = true
								And Provisioning__c = true
								And Email = :Label.AP03_OrgWideEmailAddress
								And TECH_AnonymisationStatus__c = '0'
								And LastModifiedDate < :Datetime.now().addDays(-3)];

		System.debug('## AP08_ReminderUserValidationEmail listUsers ' + listUsers);

		List<User> listUsersToReminder = new List<User>();
		for(User us : listUsers){
			us.Email = AP07_EncryptionDecryption.decrypte(us.TECH_EncryptedEmail__c);
			listUsersToReminder.add(us);
		}

		System.debug('## AP08_ReminderUserValidationEmail listUsersToReminder ' + listUsersToReminder);

		Database.update(listUsersToReminder);
	}
}