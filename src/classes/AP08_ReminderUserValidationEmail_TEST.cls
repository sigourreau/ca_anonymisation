/******************************************************************************
*   @author Simon Gourreau
*   @date 27/09/2017
*   @description AP08_ReminderUserValidationEmail test class
*
*/
@isTest
private class AP08_ReminderUserValidationEmail_TEST {
	@testSetup 
	static void dataCreation() {
		User adminUser = DataFactory.getAdminUser('testadmin@ReminderUserValidationEmail.com');
		Database.insert(adminUser);

		/*User chatterUser = DataFactory.getChatterUser('testchatter@ReminderUserValidationEmail.com');
		chatterUser.Provisioning__c = true;
		chatterUser.TECH_AnonymisationStatus__c = '0';
		chatterUser.Email = Label.AP03_OrgWideEmailAddress;
		Database.insert(chatterUser);*/
	}

	static testMethod void AP08_ReminderUserValidationEmail_TEST() {
		User adminUser = [Select Id From User Where UserName = 'testadmin@ReminderUserValidationEmail.com'];
		System.runAs(adminUser){
			Test.startTest();
		    System.schedule('AP08_ReminderUserValidationEmail', '0 0 0 * * ?', new AP08_ReminderUserValidationEmail());
		    Test.stopTest();
		}
	}
}