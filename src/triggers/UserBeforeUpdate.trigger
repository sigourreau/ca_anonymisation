/******************************************************************************
*   @author Simon Gourreau
*   @date 20/09/2017
*   @description Trigger on before update user object
*
*/
trigger UserBeforeUpdate on User (before update) {
	if(PAD.canTrigger('AP01_EncryptionDecryption')){
		if(Trigger.isBefore && Trigger.isUpdate){
        	//AP01User.anonymizeUsers(Trigger.new, Trigger.oldMap, Trigger.newMap);
        	AP01User.unanonymizeUsers(Trigger.new, Trigger.oldMap, Trigger.newMap);
			AP01User.preAnonymizeUsers(Trigger.new, Trigger.oldMap, Trigger.newMap);
		}
    }
}